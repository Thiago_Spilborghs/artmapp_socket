#!/usr/bin/python 

import socket
import rospy
from std_msgs.msg import String, Bool

class Server():
	def __init__(self):
		self.host = str(rospy.get_param('~HOST'))
		self.port = int(rospy.get_param('~PORT'))
		self.Send = ''
		self.Start = False
		self.serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		rospy.Subscriber('/Info',String,self.callback)
		while True:
			try:
				self.execute()
			except KeyboardInterrupt:
				exit()

	def callback(self,data):
		self.Send = data.data
		if str(self.Send) != '':
			rospy.loginfo(self.Send)
			self.Start = True
			self.execute()
		else:
			self.Start = False

	def execute(self):
		if self.Start == True:
			rospy.loginfo("Ready to send data")
			try:
				self.serv.connect((self.host,self.port))
				rospy.loginfo("Sending info")
				self.Send = 'Transmitir|'+self.Send
				self.serv.send(self.Send.encode())
				self.serv.close()
			except socket.error as e:
				rospy.loginfo("Client error while sending info, error %s"%e)
				self.serv.close()
			except KeyboardInterrupt:
				self.serv.close()
				exit()
			self.Start = False
		return

if __name__ == "__main__":
	rospy.init_node('ServerSocket', anonymous=True)
	Server()
	try:
		rospy.spin()
	except KeyboardInterrupt:
		print("Shutting down")
