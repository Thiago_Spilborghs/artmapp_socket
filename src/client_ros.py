#!/usr/bin/python 

import socket
import rospy
from std_msgs.msg import String

def divide_msg(msg):
	x = msg.split('|')
	return x
	
class Client():
	def __init__(self):	
		self.serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.host = rospy.get_param('~HOST')
		self.port = rospy.get_param('~PORT')
		self.Listen = False
		self.pub = rospy.Publisher('/goal',String,queue_size=1)
		self.pub2 = rospy.Publisher('/proximity',String,queue_size=1)
		self.pub3 = rospy.Publisher('/extra',String,queue_size=1)		
		self.serversocket.bind((self.host, self.port))
		self.serversocket.listen(10)
		self.conn, self.addr = self.serversocket.accept()
		while True:
			self.execute()
			if self.Listen == True:
				self.serversocket.bind((self.host, self.port))
				self.serversocket.listen(10)
				self.conn, self.addr = self.serversocket.accept()
				self.Listen = False


	def execute(self):
		try:
			print 'Socket listening'
			data = self.conn.recv(1024)
			if data:
				data = data[0]
				x = divide_msg(data)
				self.pub.publish(x[1])
				self.pub.publish(x[0])
				self.pub.publish(x[2])
				  
		except socket.timeout as e:
			self.serversocket.close()
			self.Listen = True
			rospy.loginfo(e)
			return
		except socket.error as e:
			rospy.loginfo('Error on Server: %s'% e)
			self.Listen = True
			self.serversocket.close()
		except KeyboardInterrupt:
			self.serversocket.close()
			self.Listen = True
			exit()

			

if __name__ == "__main__":
	rospy.init_node('Socket_ros', anonymous=True)
	Client()
	try:
		rospy.spin()
	except KeyboardInterrupt:
		print("Shutting down")
